import pandas as pd
import pandas.io.sql as sqlio
import xlwings as xw
import psycopg2 as pg2
from psycopg2 import Error 
import os
import shutil
import time
from queries import data_query, check_company_query

def db_conn(db_name):
    db_test_names = ['pruebas','demo','sandbox']

    if db_name == 'replica':
        conn = pg2.connect(
                host="hcmdb2replica.c1lonu8qjuki.us-east-1.rds.amazonaws.com",
                database="competences",
                user="innpulsa",
                password="innpulsa"
            )
        return conn

    elif db_name in db_test_names:
        conn = pg2.connect(
                host="feedback-db.c1lonu8qjuki.us-east-1.rds.amazonaws.com",
                database=db_name,
                user="innpulsa",
                password="innpulsa"
            )
        return conn

    else:
        return "DB_name_not_found"

def get_data(company_id, db_name):
    try:
        conn = db_conn(db_name)

        #Cursor to perform DB operations
        cur = conn.cursor()

        #Get starting time
        start = time.time()

        #Copy Template
        src_dir = os.getcwd()
        src_file = src_dir + r"\template\template.xlsx"
        #Rename the file and Copy into the same folder
        new_filename = f'Reporte_reclutamiento_{company_id}_' + time.strftime("%Y%m%d%H%M") + '.xlsx'
        copy_file = src_dir + "\\template\\" + new_filename
        shutil.copyfile(src_file , copy_file)
        #Destination directory 
        dest_file_renamed = src_dir + "\\reports\\" + new_filename
        shutil.move(copy_file, dest_file_renamed)

        #Get data
        data = data_query(company_id)
        data_df = sqlio.read_sql_query(data, conn)

        #Write in Excel
        print('\nOpening template')
        try:
            app = xw.App(visible=False)
            wb = app.books.open(dest_file_renamed)

            wb_data = wb.sheets['Datos']

            print("Writing data")
            wb_data["A2"].options(pd.DataFrame, header=False, index=False, expand='table').value = data_df

            wb.save()
            app.quit()

            #Save ending time
            end = time.time()
            total_time = (end - start) / 60
            time_minutes = float("{:.2f}".format(total_time))
            print(f"Elapsed time: {time_minutes} minutes")

            return new_filename
        except:
            app.quit()
            print("An error occurred, no data was inserted")

    except (Exception, pg2.DatabaseError) as error:
        print(error)

def check_data(company_id, db_name):
    conn = db_conn(db_name)
    if conn == "DB_name_not_found":
        result = conn
    else:
        #Cursor to perform DB operations
        cur = conn.cursor()
        cur.execute(check_company_query(company_id))
        result = cur.fetchall()

    return result