from fastapi import FastAPI
from fastapi import Path, Query
from typing import Optional

from get_data import get_data, check_data
from boto import upload_s3

app = FastAPI()

@app.get("/healthcheck")
def read_root():
    return {"Hello": "Acsendo from Python and FastAPI"}

@app.get("/reclutamiento/v1/es/{db}/{company_id}")
async def home(
    company_id: int = Path(..., gt=0),
    db: str = Path(...),
    token: Optional[int] = Query(
        default=None,
        ge=1
    )
):
    company_data = check_data(company_id, db)

    if company_data == "DB_name_not_found":
        return{"Error": "DB name is not valid"}

    elif not company_data:
        return{"Error": "Company ID doesn't exist"}

    else:
        report = get_data(company_id, db)
        s3_link = upload_s3(report)

        object_link = f'https://{s3_link}.s3.amazonaws.com/reclutamiento/{report}'
        
        return{"Success": "Report generated successfully",
                "URL": object_link}