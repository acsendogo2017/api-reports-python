def data_query(company_id):
    data = f""" SELECT c."name" AS "Nombre", 
            d.name AS "Departamento", 
            v."name" AS "Nombre posición", 
            v.state AS "Estado de la oferta", 
            v.createddate::timestamp::date AS "Fecha de creación", 
            rp.application_platform AS "Fuente de entrada candidato",
            CASE 
                WHEN v.contract_type = 'APPRENTICESHIP' THEN 'Aprendizaje'
                WHEN v.contract_type = 'PROVISION_OF_SERVICES' THEN 'Prestación de servicios'
                WHEN v.contract_type = 'INDEFINITE_TERM' THEN 'Término indefinido'
                WHEN v.contract_type = 'SEASONAL' THEN 'Temporal'
                WHEN v.contract_type = 'FIXED_TERM' THEN 'Término definido'
                ELSE v.contract_type  
            END AS "Tipo de contrato",
            CASE 
                WHEN s."name" = 'PROFILE' THEN 'Perfil'
                WHEN s."name" = 'INTERVIEW' THEN 'Entrevista'
                WHEN s."name" = 'SELECTION' THEN 'Selección'
                WHEN s."name" = 'TESTS' THEN 'Pruebas'
                ELSE s."name" 
            END AS "Etapa de la oferta",
            CASE 
                WHEN rp.postulation_state = 'IN_PROCESS' THEN 'En proceso'
                WHEN rp.postulation_state = 'REJECTED' THEN 'Rechazado'
                WHEN rp.postulation_state = 'APPROVED' THEN 'Aprobado'
                ELSE rp.postulation_state 
            END AS "Estado postulacion"
            FROM recruiting.recruiting_postulation rp
            JOIN recruiting.candidate c ON rp.candidate_id = c.id
            JOIN recruiting.stage_company sc ON rp.stage_company_id = sc.id
            JOIN recruiting.stage s ON sc.stage_id = s.id
            JOIN recruiting.vacant v ON rp.vacant_id = v.id
            JOIN hcm.division d ON v.division_id = d.id
            JOIN hcm.company c2 ON v.company_id = c2.id
            WHERE rp.state = 'ACTIVE'
            AND v.company_id = {company_id}; """
    
    return data

def check_company_query(company_id):
    check = f""" SELECT DISTINCT v.company_id 
            FROM recruiting.vacant v 
            WHERE v.company_id = {company_id}; """
            
    return check
