## Install Libraries
To install requirements.txt run the following command

     pip install -r requirements.txt

Python version required:

    Python 3.9.7

CREATE IMAGE FOR RUN APP PYTHON (DOCKER)

-Run file Dockerfile and built image: docker build --no-cache=true -t name_image:latest .
-Now, Run container from image create: docker run -d --name name_container -p 8000:8000 name_image:latest